# Use the Ruby Home HAP Server to create Screenline Blinds

## Issues

The Home app shows a spinner while the blinds are opening or closing. Updating the
position during this does not show a percentage, even when long pressing to the indicator.

The indicator, once set doesn't update.

Setting current_position does not update the slider. If target and current aren't the same
then the spinner spins

Setting current isn't an option, set target. So if blind moves from external source you
must update target not current.

If you click on the Home app whilst the blind is closing a target position event is queued
and is handled after the previous one completes. So you need a thread to do the closing.

Note that when the target updates unless you set the current position to it then the
spinner keeps spinning

# Installation

## MacOS

Install ruby and bundler using `brew install ruby bundler`

Determine the absolute path (with no symlinks) to the `bundle` executable.

Compile the `rgs` script from the `rgpio-tools` package and ensure that
it is in the path.

Edit the file `net.sendium.homekit-screenline.plist` to reflect the actual path to the
`bundle` executable, the correct working directory and the address of the remote `rgpiod` server.
Copy the file into the `~/Library/LaunchAgents` folder and issue the command:

    launchctl bootstrap gui/501 ~/Library/LaunchAgents/net.sendium.homekit-screenline.plist

You will then be prompted (in a GUI session) to allow bundler and ruby full disk access and network access.
Grant those.

### Issues

Use the command `launchctl print gui/501/net.sendium.homekit-screenline` to see the state:

    * Last exit code 78 (EX_CONFIG) is a general result if:
        *  the executable has symlinks or
        *  stdout/stderr is not specified or is on a mounted volume
    * If the process is running but nothing is happening it is likely suspended waiting on
      permissions for disk and network access to be granted. These appear in a GUI
      session.

## Ubuntu

In Ubuntu you need to add the following packages then run bundler:

    apt-get install ruby-bundler ruby-dev build-essential libavahi-compat-libdnssd-dev
    bundle install

When using the remote `rgpiod` daemon set the environment variable LG_ADDR to the remote server.
If using systemd set this variable in the unit file `/etc/systemd/system/homekit-screenline.service`

## Accessing GPIO and Local vs. Remote

The interface to the GPIO pins on a Raspberry Pi changed from kernel version 5.1 and up so
ensure that the correct script is listed in the configuration YAML file.
The older pigpio interface uses the script `switch.py` which requires specific python packages and device permissions.
We are now using the newer lgpio interface with the script `press_pin`. On the server side this uses the `rgs` shell client:

    apt-get install rgpio-tools

Which requires the remote access daemon `rpgiod` to be installed on the remote machine:

    apt-get install rgpiod

Then update the `/etc/defaults/rgpiod` file to remove the `-l` option. This allows remote
access. Finally enable it at boot time with:

    systemctl enable rgpiod

# Problems

## Blinds get out of Sync

Use the `fixposition` script to set them to the position that HomeKit thinks they are in.
Alternatively use the remote.

## A blind stops responding

The controlling thread exits, not sure why.

## Doesn't Appear on the Add Accessory screen in Homekit

This can happen when you've deleted the bridge from HomeKit but have kept the
old `accessory_info.yaml` and `identifier_cache.yaml` files. I'm guessing that HomeKit
won't add the same identifiers after they've been deleted. To 'Reset' the bridge delete
those files and add all the blinds back to HomeKit.
