# Implement the Blind Class
class Blind

  attr_accessor :ticking
  attr_reader   :movethread, :name

  TILT_QUANTA = 4

  POSITION_STATE = {
    0 => "Closing",
    1 => "Opening",
    2 => "Stopped"
  }
  # divide the louvre range (180) into pieces
  # return the divided point closest to the tilt
  #
  def self.quantize tilt
    quantum = 180.0 / TILT_QUANTA
    quanta = ((tilt + 90) / quantum).round(0)
    return [(quanta * quantum) - 90, quanta]
  end

  def initialize blind, opts, store
    @logger = Logger.new(STDOUT, formatter: proc {|sev, dt, prog, msg| "#{dt.strftime('%H:%M:%S.%L')} #{sev} #{blind[:name]} : #{msg}\n" })
    @logger.level = (opts[:debug] or blind[:debug]) ? :debug : :info

    @name = blind[:name]
    @store = store
    @opts = opts
    @store.transaction do
      @state = @store[name]
    end
    @logger.info "#{name}: pins #{blind[:pins][:up]}, #{blind[:pins][:dn]}"
    @pins = blind[:pins]
    @blind = RubyHome::ServiceFactory.create(:window_covering,
      name: name,
      target_position: @state[:position],
      current_position: @state[:position],
      position_state: 2,
      current_horizontal_tilt_angle: @state[:tilt],
      target_horizontal_tilt_angle: @state[:tilt])

    @blind.target_position.after_update do |target_position|
      @logger.info "TARGET UPDATE: window covering target_position.after_update #{target_position}"
      @store.transaction do
        @state[:position] = target_position
        @store[blind[:name]] = @state
        @store.commit
      end
      @update_position = true
    end

    @blind.position_state.after_update do |position_state|
      position_state = POSITION_STATE[position_state]
      @logger.debug "position state #{position_state}"
    end

    @blind.current_position.after_update do |current_position|
      @logger.debug "current position #{current_position}"
    end

    @blind.current_horizontal_tilt_angle.after_update do |current_horizontal_tilt_angle|
      @logger.debug "current_horizontal_tilt_angle #{current_horizontal_tilt_angle}"
    end

    @blind.target_horizontal_tilt_angle.after_update do |target_horizontal_tilt_angle|
      @logger.debug "target_horizontal_tilt_angle #{target_horizontal_tilt_angle}"
      @store.transaction do
        @state[:tilt] = target_horizontal_tilt_angle
        @store[name] = @state
        @store.commit
      end
      @update_tilt = true
    end

    @ticking = true
    @update_position = false
    @update_tilt = false
    @movethread = Thread.new do
      begin
        tickTock(opts[:tick])
      rescue
        @logger.warn "The thread for blind #{blind[:name]} has died!\n#{$!}"
        @logger.warn $!.backtrace
      end
    end

  end

  def saveState position, tilt

  end

  # Issue the command to the GPIO pins
  #
  def command(pin, duration, iterations = 1)
      cmd = "#{@opts[:script]} #{pin} #{duration}"
      iterations.times{ @logger.debug(`#{cmd}`) }
  end

  def motionDuration delta, current_tilt
    move_duration = @opts[:durations][:move] * delta.abs / 100.0
    tilt_reset = (delta <=> 0) * 90 - current_tilt
    tilt_duration = @opts[:durations][:tilt] * tilt_reset.abs / 180.0
    @logger.debug "motionDuration: delta = #{delta}, tilt_duration = #{tilt_duration}, move_duration = #{move_duration}"
    return (move_duration + tilt_duration)/1000
  end

  def stopBlinds delta, pos
    direction = delta < 0 ? :dn : :up
    opposite = delta > 0 ? :dn : :up
    @logger.info "Stopping #{direction}wards move at position #{pos}"
    if [0,100].include? pos
      @logger.debug "stop not needed at #{pos}"
      if pos == 100
        @blind.current_horizontal_tilt_angle = 0
        @blind.target_horizontal_tilt_angle = 0
      end
    else
      command(@pins[opposite], @opts[:durations][:stop_cmd])
    end
    @blind.current_position = pos
    @blind.position_state = 2
  end

  # Moving the blinds has the side effect of altering the tilt, so return the new tilt angle
  def moveBlinds delta
    if delta > 0 # moving up (open)
      new_tilt = 90
      @logger.debug "moving blind #{delta} (up)"
      @blind.position_state = 1 # going to maximum
      command(@pins[:up], @opts[:durations][:move_cmd])
    else
      new_tilt = -90
      @logger.debug "moving blind #{delta} (down)"
      @blind.position_state = 0 # going to minimum
      command(@pins[:dn], @opts[:durations][:move_cmd])
    end
    @blind.target_horizontal_tilt_angle = new_tilt
    @blind.current_horizontal_tilt_angle = new_tilt
    return new_tilt
  end

  # Tilt the blind and return the tilt angle
  def tiltBlinds current_tilt, target_tilt
    if @blind.current_position.value == 100
      @logger.debug "Ignoring tilt: blind is fully up"
      # Although we set the current angle, the UI slider doesn't update
      @blind.current_horizontal_tilt_angle = current_tilt
      return current_tilt
    end
    q_tilt, tq = Blind.quantize(target_tilt)
    q_current_tilt, cq = Blind.quantize(current_tilt)
    @logger.debug "Quantized current tilt #{q_current_tilt}, target tilt #{q_tilt}"
    q_tilt_delta = q_tilt - q_current_tilt
    tilt_iterations = (tq - cq).abs
    pin, duration = case q_tilt_delta <=> 0
          when 1 then [@pins[:up], @opts[:durations][:tilts][:up]]
          when -1 then [@pins[:dn], @opts[:durations][:tilts][:dn]] when 0 then [nil, nil]
          end
    if pin
      @logger.info "Tilting blind: delta #{q_tilt_delta}"
      command(pin, duration, tilt_iterations)
      @blind.current_horizontal_tilt_angle = q_tilt
      @blind.target_horizontal_tilt_angle = q_tilt
    end
    return q_tilt
  end

  def tickTock interval
    start_pos = nil
    start_time = nil
    end_time = nil
    end_pos = nil
    delta = nil
    calc_pos = nil
    current_tilt = @blind.current_horizontal_tilt_angle.value
    save_tilt = nil
    while @ticking do
      sleep interval/1000
      t = Time.now
      if @update_tilt
        if end_time
          @logger.debug "Tilt request ignored, blinds are moving!"
        else
          target_tilt = @blind.target_horizontal_tilt_angle.value
          @logger.debug "Tilt requested: current #{current_tilt}º, target #{target_tilt}º"
          current_tilt = tiltBlinds(current_tilt, target_tilt)
          @update_tilt = false
        end
      end
      if @update_position
        if end_time
          new_end_pos = @blind.target_position.value
          new_delta = new_end_pos - calc_pos
          end_time = t + motionDuration(new_delta, current_tilt)
          reversing = (delta < 0 && new_delta > 0 or delta > 0 && new_delta < 0) ? "(reversing) ": ""
          @logger.info "Updating motion #{reversing}to position #{new_end_pos}, end_time now #{end_time.ctime}"
          if reversing.size > 0
            current_tilt = moveBlinds(new_delta)
          end
          @update_tilt = false # a tilt does not need triggering after a move (unless a tilt is saved)
          delta = new_delta
          end_pos = new_end_pos
          start_pos = calc_pos
          start_time = t
        else # start a new motion
          start_pos = @blind.current_position.value
          end_pos = @blind.target_position.value
          @logger.debug "starting a new motion from #{start_pos} to #{end_pos} at tilt #{current_tilt}"
          delta = end_pos - start_pos
          save_tilt = current_tilt unless [-90,90].include? current_tilt
          if delta != 0
            motion_duration = motionDuration(delta, current_tilt)
            start_time = t
            end_time = start_time + motion_duration
            @logger.info "Beginning motion: delta = #{delta}, duration = #{motion_duration}, end_time = #{end_time.ctime}"
            current_tilt = moveBlinds(delta)
            @update_tilt = false # a tilt does not need triggering after a move (unless a tilt is saved)
          end
        end
        @update_position = false
      end
      if end_time
        if t > end_time
          stopBlinds(delta, end_pos)
          end_pos = nil
          end_time = nil
          if save_tilt
            @logger.debug "restoring tilt #{save_tilt} (from #{current_tilt})"
            tiltBlinds(current_tilt, save_tilt)
            save_tilt = nil
            @update_tilt = false
          end
        else
          calc_pos = start_pos + (delta * (t - start_time)/(end_time - start_time)).to_i
          @logger.debug "moving: calc pos is #{calc_pos}"
        end
      end
    end
    @logger.info "Thread exiting"
  end

end
