#!/usr/bin/env python3
# Turn on a GPIO pin for a time interval (milliseconds)
#
import lgpio
import os
import re
import sys
import time

h = 0
chip = 4

try:
    if len(sys.argv) < 2:
        raise ValueError
        
    if re.match("\d{1,2}$", sys.argv[1]):
        pin = int(sys.argv[1])
    else:
        raise ValueError

    if len(sys.argv) == 3:
        m = re.match("^[0-9]*$", sys.argv[2])
        if m is None:
            raise ValueError
        dur = m.group(0)
    else:
        dur = "2000"

    duration = float(dur)

    if os.environ.get("DEBUG"):
        print(f"Pin: {pin}, duration {duration}s")

    h = lgpio.gpiochip_open(chip)
    lgpio.gpio_claim_output(h, pin)
    lgpio.gpio_write(h, pin, 1)
    time.sleep(duration/1000)
    lgpio.gpio_write(h, pin, 0)

except KeyboardInterrupt:
    lgpio.gpio_write(h, pin, 0)

except ValueError:
    print("Usage: {sys.argv[0]} pin [duration (mS)]")
    sys.exit(2)

finally:
    if h != 0:
        lgpio.gpiochip_close(h)
    
