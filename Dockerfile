FROM arm64v8/ubuntu:rolling

ARG S6_OVERLAY_VERSION=3.0.0.2-2

RUN apt-get update && \
    DEBIAN_FRONTEND=noninteractive apt-get -y install \
      avahi-daemon \
      build-essential \
      libavahi-compat-libdnssd-dev \
      libffi-dev \
      liblgpio1 \
      libnss-mdns \
      libsodium-dev \
      ruby-dev \
    && \
    gem install -N ruby_home && \
    apt-get autoremove --purge -y build-essential && \
    apt-get -y clean && \
    rm -rf /var/lib/apt/lists/*

RUN useradd -u 911 -U -d /srv/runuser -s /bin/false abc && \
    usermod -G users abc && \
    mkdir -p /srv/rubyhome

ADD https://github.com/just-containers/s6-overlay/releases/download/v${S6_OVERLAY_VERSION}/s6-overlay-noarch-${S6_OVERLAY_VERSION}.tar.xz /tmp
RUN tar -C / -Jxpf /tmp/s6-overlay-noarch-${S6_OVERLAY_VERSION}.tar.xz
ADD https://github.com/just-containers/s6-overlay/releases/download/v${S6_OVERLAY_VERSION}/s6-overlay-aarch64-${S6_OVERLAY_VERSION}.tar.xz /tmp
RUN tar -C / -Jxpf /tmp/s6-overlay-aarch64-${S6_OVERLAY_VERSION}.tar.xz
COPY rootfiles /
COPY screenline.rb /srv/rubyhome/
WORKDIR /srv/rubyhome
VOLUME /srv/rubyhome

CMD ["/srv/rubyhome/screenline.rb"]

ENTRYPOINT ["/init"]
