#!/usr/bin/env ruby
#
# A screenline blinds louvre behaviour is:
# - it comes down at -90 (slat up towards inside)
# - it goes up at +90 (slat up towards outside)
# - at the top it is 0
# - at the bottom it is -90 and has to transition to +90 to rise
# - when halfway it has to transition if going the opposite way.
#
require 'ruby_home'
require 'optparse'
require 'yaml'
require 'yaml/store'

require_relative "blind"

$stdout.sync = true

opts = { debug: false, config_file: "#{__dir__}/config.yml", initial_pos: 0, initial_tilt: -90 }

OptionParser.new do |o|
    o.banner = "Usage: screenline ..."
    o.separator ""
    o.separator "Start the HAP server for Screenline Blinds"

    o.on( '-d', '--debug', "Debug (default #{opts[:debug]})" ) do
      opts[:debug] = true
    end
    o.on( '-f', '--config CONFIG', "YAML configuration file (default #{opts[:config_file]})" ) do |f|
      f = "#{__dir__}/#{f}" unless File.absolute_path?(f)
      raise "Configuration file '#{f}' does not exist" unless File.exist?(f) && ! File.directory?(f)
      opts[:config_file] = f
    end
    o.on( '-p', '--position POS', "Initial position (0=closed, 100=open) (default #{opts[:initial_pos]})" ) do |p|
      pos = p.to_i
      raise "Position #{pos} is out of range" unless (0..100).include? pos
      opts[:initial_pos] = pos
    end
    o.on( '-s', '--script SCRIPTFILE', "Script to move the blinds (no default})" ) do |s|
      opts[:script] = s
    end

    o.on( '-a', '--angle ANGLE', "Initial tilt angle (-90 fully down, 90 fully up) (default #{opts[:initial_tilt]})" ) do |a|
      angle = a.to_i
      raise "Tilt angle #{angle} is out of range" unless (-90..90).include? angle
      opts[:initial_tilt] = angle
    end
    o.on( '-?', '--help', 'Display this screen' ) do
        puts o
        exit
    end
    begin
      o.parse!
      raise "When blind is at the top the tilt must be 90!" if opts[:initial_pos] == 100 and opts[:initial_tilt] != 90
    rescue
      puts "Error: #{$!}"; puts o; exit
    end
end

begin
  config = YAML.load(File.open(opts[:config_file],"r",&:read))
  opts = config[:opts].merge(opts)
  opts[:hostname] ||= `hostname`.strip
rescue
  puts "Error in configuration file #{opts[:config_file]}:\n#{$!}"
  exit 2
end

opts[:script] = "#{__dir__}/#{opts[:script]}" unless File.absolute_path?(opts[:script])

unless File.executable?(opts[:script])  && ! File.directory?(opts[:script])
  puts "Script file '#{opts[:script]}' is not executable"
  exit 2
end

puts "Options are: #{opts.inspect}" if opts[:debug]

RubyHome.configure do |c|
  c.discovery_name = opts[:discovery_name]
  c.password = opts[:password]
  c.category_identifier = :window_covering
  c.port = 16066
end

RubyHome::ServiceFactory.create(:accessory_information,
  firmware_revision: '16.1.61',
  manufacturer: 'Screenline',
  model: 'ST2121',
  name: 'Screenline Blinds',
  serial_number: "HKSL-#{opts[:hostname]}",
  category_identifier: :window_covering
)

store = YAML::Store.new("config.store", true)
store.transaction do
  for blind in config[:blinds]
      name = blind[:name]
      if store.root?(name)
        state = store[name]
        puts "Blind #{name} state restored: #{state[:position]}% tilt #{state[:tilt]}º"
      else
        puts "Blind #{name} state from command line: #{opts[:initial_pos]}% tilt #{opts[:initial_tilt]}º"
        state = { position: opts[:initial_pos], tilt: opts[:initial_tilt] }
        store[name] = state
      end
  end
  store.commit
end

blinds = []
for blind in config[:blinds]
  b = Blind.new(blind, opts, store)
  blinds << b
end

begin
  RubyHome.run # This doesn't return
rescue
  puts "Exception in RubyHome.run: #{$!}"
end

blinds.each do |b|
  b.ticking = false
  b.movethread.join
end
